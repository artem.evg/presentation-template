const gulp = require('gulp');
const image = require('gulp-image');

module.exports = function imageMin() {
    return gulp.src('src/img/**/*.{gif,png,jpg,svg,webp}')
        .pipe(gulp.dest('build/img'));
}
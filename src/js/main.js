// const vh = window.innerHeight * 0.01;
// const body = document.querySelector('body')
// document.documentElement.style.setProperty('--vh', `${vh}px`);
// window.addEventListener('resize', () => {
// 	document.documentElement.style.setProperty('--vh', `${vh}px`);
// })

function logicApp () {
	const wrapper = document.querySelector('.app-wrapper');
	const pages = document.querySelectorAll('.app-page');
	let colors = ['#a28ac2', '#c99db8', '#97b082', '#dbde9b', '#db925e', '#e09080', '#a270d4', '#66bdb7', '#2c2f5e'];
	let col = 5;

	gsap.set(wrapper, {
		x: 0,
		y: 0
	})
	gsap.set('.app-page', {
		width: `${100 / col}%`
	})

	for (let i = 0; i < pages.length; i++) {
		let page = pages[i];
		page.style.backgroundColor = colors[i];
		page.textContent = i + 1;

		// Влево
		let left = document.elementFromPoint(page.offsetLeft - page.clientWidth + 1, page.offsetTop + 1);
		if (left && left.closest('.app-page') && !left.closest('.ignore')) {
			let button = document.createElement('button');
			button.classList.add('left');
			button.textContent = 'left';
			button.relatedElement = left;
			page.append(button);
		}

		// Вправо
		let right = document.elementFromPoint(page.offsetLeft + page.offsetWidth + 1, page.offsetTop + 1);
		if (right && right.closest('.app-page') && !right.closest('.ignore')) {
			let button = document.createElement('button');
			button.classList.add('right');
			button.textContent = 'right';
			button.relatedElement = right;
			page.append(button);
		}

		// Вверх
		let top = document.elementFromPoint(page.offsetLeft + 1, page.offsetTop - page.clientHeight + 1);
		if (top && top.closest('.app-page') && !top.closest('.ignore')) {
			let button = document.createElement('button');
			button.classList.add('top');
			button.textContent = 'top';
			button.relatedElement = top;
			page.append(button);
		}

		// Вниз
		let bottom = document.elementFromPoint(page.offsetLeft + 1, page.offsetTop + page.offsetHeight + 1);
		if (bottom && bottom.closest('.app-page') && !bottom.closest('.ignore')) {
			let button = document.createElement('button');
			button.classList.add('bottom');
			button.textContent = 'bottom';
			button.relatedElement = bottom;
			page.append(button);
		}
	}

	function scrollToElem (el) {
		gsap.to(wrapper, {
			x: el.offsetLeft * -1,
			y: el.offsetTop * -1
		})
	}

	let timeout;

	wrapper.addEventListener('click', (e) => {
		let button = e.target.closest('button');
		if (button === null) return;
		if (!button.relatedElement) return
		scrollToElem(button.relatedElement)
	})

	let yPos;

	window.addEventListener("touchstart", (e) => {
		console.log(e, 'start')
		yPos = e.changedTouches[0].clientY;
	});
	window.addEventListener("touchend", (e) => {
		console.log(e, 'end')
		yPos = e.changedTouches[0].clientY - yPos;
		let wheelEvent = new Event('wheel', {bubbles: true, cancelable: true});
		wheelEvent.deltaY = yPos;
		e.target.dispatchEvent(wheelEvent)
	});

	window.addEventListener('wheel', (e) => {
		clearTimeout(timeout)
		timeout = setTimeout(() => {
			let page = e.target.closest('.app-page');

			if(Math.sign(e.deltaY) === -1) {
				let btn = page.querySelector('.top') || page.querySelector('.left');
				if (btn) {
					btn.click()
				}
			}

			if(Math.sign(e.deltaY) === 1) {
				let btn = page.querySelector('.bottom') || page.querySelector('.right');
				if (btn) {
					btn.click()
				}
			}
		}, 100)
	})

	setTimeout(() => {
		wrapper.style.width = `${window.innerWidth * col}px`;
		pages.forEach(page => page.style.height = '100vh');
		wrapper.style.opacity = 1
	}, 100);
}

window.addEventListener('DOMContentLoaded', logicApp)
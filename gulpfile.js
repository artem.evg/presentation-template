const gulp = require('gulp')

const sync = require('./gulp/tasks/sync')
const pug2html = require('./gulp/tasks/pug2html')
const styles = require('./gulp/tasks/styles')
const minifyJs = require('./gulp/tasks/minifyJs')
const includesJs = require('./gulp/tasks/includesJs')
const notMinifyJs = require('./gulp/tasks/notMinifyJs')
const fonts = require('./gulp/tasks/fonts')
const image = require('./gulp/tasks/image.js')
const video = require('./gulp/tasks/video')
const cleaner = require('./gulp/tasks/cleaner')

function setMode(isProduction = false) {
	return cb => {
		process.env.NODE_ENV = isProduction ? 'production' : 'development'
		cb()
	}
}

const dev = gulp.parallel(pug2html, styles, fonts, image, video, includesJs, minifyJs, notMinifyJs)

const build = gulp.series(cleaner, dev)

module.exports.start = gulp.series(setMode(), build, sync)
module.exports.build = gulp.series(setMode(true), build)